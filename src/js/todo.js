"use strict";
(() => {
    
    const listContainerEl = document.querySelector(".list-items");
    const removeItemBtn = document.querySelector(".list-item-object span");
    const listItem = document.querySelector(".list-items");
    const itemInput = document.querySelector(".item-box");
    let close;
    let itemsToSave = [];
       
    //focus main input
    itemInput.focus();
    //Defining how we"re going to respond to setting, and getting events on our array.
    const arrayChangeHandler = {
        get: function (target, event) {
            if (target != null) {
                //do stuff
            }
            return target[event];
        },
        //When a value is set (added, pushed) we will saveList() and output a console response
        set: function (target, property, value, receiver) {
            target[property] = value;
            saveList();
            console.log("Item saved: ", itemsToSave[itemsToSave.length - 1]);
            return true;
        }
    };
    //Enable the itemsToSave array to be exposed to the arrayChangeHandler, using a proxy
    const proxyArray = new Proxy(itemsToSave, arrayChangeHandler);
    //addItem(param) method that accepts the item to be added into our proxified array
    let addItem = (item, fromStorage) => {
        if (item != "") {
            let newItem = createListItem(item);
            let closeBtn = createCloseForListItem(item, newItem);
            //Create event listener on newly created remove icon
            closeBtn.addEventListener("click", () => {
                removeListItem(item, newItem, closeBtn.getAttribute("item"))
            });
            newItem.addEventListener("keydown", (evt) => {
                if(evt.keyCode == 13 && item !== evt.target.value){
                    updateListItem(item, evt.target.value);
                    item = evt.target.value;
                }
            })
            if (!fromStorage) {
                proxyArray.push(item);
            } else {
                //Don"t use proxy when adding to array via storage. why? becase we have a saveList() call on the proxy that we don"t want to call.
                itemsToSave.push(item);
            }
            return newItem; //Returns newly created list item
        }
        showAlert("Item can't be empty!", 2500);
        return false;
    }
    //enter event for the add item element
    itemInput.addEventListener("keydown", (evt) => {
        if (evt.keyCode == 13) {
            addItem(itemInput.value, false);
            itemInput.value = "";
        }
    });

    //Remove list items on page, and also clear local storage array
    let reset = () => {
        while (listItem.firstChild) {
            listItem.removeChild(listItem.firstChild);
        }
        localStorage.removeItem("list");
        itemsToSave.length = 0;
    }
    //Get saved array from local storage and add items to UI
    let loadSavedItems = () => {
        if (getList() != null) {
            for (let i = 0; i < getList().length; i++) {
                addItem(getList()[i], true);
            }
        }
    }
    //Save array to local storage
    let saveList = () => {
        localStorage.setItem("list", JSON.stringify(itemsToSave));
    }
    //Get array from local storage
    let getList = () => {
        return JSON.parse(localStorage.getItem("list"));
    }
    //Make span element on newly created list element
    let createCloseForListItem = (item, parentEl) => {
        let closeBtn = parentEl.appendChild(document.createElement("span"));
        closeBtn.innerHTML = "✕";
        closeBtn.setAttribute("item", item);
        closeBtn.classList.add("rmv-list");
        return closeBtn;
    }
    //Creates the list item, as a div with an input as the child. Adds attributes to elements for easily keeping track of each list item
    let createListItem = (itemContent) => {
        let itemCont = listContainerEl.appendChild(document.createElement("div"));
        let listBox = itemCont.appendChild(document.createElement("input"));
        itemCont.classList.add("lb-box-item");
        listBox.setAttribute("type", "text");
        listBox.value = itemContent;
        listBox.classList.add("list-item-object");
        itemCont.setAttribute("item", itemContent);
        return itemCont;
    }
    //Remove a list item from the DOM and list item array [then updates localstorage]
    let removeListItem = (item, parent, closeBtnItem) => {
        let children = parent.childNodes;
        if (parent.firstChild) {
        parent.classList.add("remove-list");
        parent.removeChild(parent.firstChild);
            for (let i = 0; i < itemsToSave.length; i++) {
                if (closeBtnItem == itemsToSave[i]) {
                    itemsToSave.splice(i, 1);
                    saveList();
                }
            }
        }
    }
    //updateListItem is called when a list item is edited. Locates the existing item, and replaces it in the array with the updated item.
    let updateListItem = (oldItem, newItem) => {
        if(newItem !== ""){
        for(let i = 0; i < itemsToSave.length; i++){
            if(itemsToSave[i] == oldItem){              
                itemsToSave.splice(i, 1, newItem);
                saveList();
                showAlert("Item updated!", 2500);
            }
        }
   }else{
        showAlert("You can't have an empty list!", 2500);
    }
   
}
    //returns close button elements
    let getCloseElements = () => {
        return document.getElementsByClassName('rmv-list');
    }
    let showAlert = (paramText, duration) => {
        const bar = document.querySelector('.notif__bar');
        if(!bar.classList.contains('alert-bar')) {
            bar.classList.toggle('alert-bar');
            bar.textContent = paramText;
            setTimeout(() => {
                bar.classList.remove('alert-bar');
                bar.classList.add('remove-bar');
               setTimeout(() => {
                bar.classList.remove('remove-bar');
               },500);
            }, duration);
            
        }


    
    }
    //load items from localstorage
    loadSavedItems();
})();