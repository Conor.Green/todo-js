# todo-js

Todo JS is a simple list generator. You enter something, Todo JS will display it, and remember it. Simple. Nothing more, nothing less. Oh, I lied. You can click on a list item and edit it. It uses [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) to save and retrieve the list items.

## Seriously, another todo list?

I wanted to develop something simple, and something that works without any dependencies in the hopes that it will encourge people to develop in pure JS before looking to frameworks and libraries. 

## Getting Started

You will find the source files in ```src```.

There is a gulpfile with a watch task that will compress/compile the HTML, CSS and JS.

## Installing 

Run ```npm install``` to install required modules for building/task automation

Run ```gulp watch``` to build and watch changes

## Contributing

Feel free to contribute to this project by adding new features, and fixing any bugs you may find.

## Versioning

todo-js uses [SemVer](http://semver.org/) for versioning.

## Authors

* **Conor Green** - [GitLab](https://gitlab.com/Conor.Greenh)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details






